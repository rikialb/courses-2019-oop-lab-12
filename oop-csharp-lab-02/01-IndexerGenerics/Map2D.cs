﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */

        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;

        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            foreach(var k1 in keys1)
            {
                foreach(var k2 in keys2)
                {
                    values.Add(new Tuple<TKey1, TKey2>(k1, k2), generator.Invoke(k1, k2));
                }
            }

            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            if(!(this.NumberOfElements == other.NumberOfElements))
            {
                return false;
            }
            foreach(var k in values.Keys)
            {
                if (!this[k.Item1, k.Item2].Equals(other[k.Item1, k.Item2]))
                {
                    return false;
                }
            }
            return true;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
                if(values.ContainsKey(new Tuple<TKey1, TKey2>(key1, key2)))
                    return this.values[new Tuple<TKey1, TKey2>(key1, key2)];
                else
                    throw new ArgumentException();
            }

            set
            {
                if (values.ContainsKey(new Tuple<TKey1, TKey2>(key1, key2)))
                    this.values[new Tuple<TKey1, TKey2>(key1, key2)] = value;
                else
                    this.values.Add(new Tuple<TKey1, TKey2>(key1, key2), value);
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            var row = (from valore in this.values
                      where valore.Key.Item1.Equals(key1)
                      select new Tuple<TKey2, TValue>(valore.Key.Item2, valore.Value)).ToList();
            return row;     
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            var column = (from valore in this.values
                         where valore.Key.Item2.Equals(key2)
                         select new Tuple<TKey1, TValue>(valore.Key.Item1, valore.Value)).ToList();
            return column;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            return  (from valore in this.values
                    select new Tuple<TKey1, TKey2, TValue>(valore.Key.Item1,valore.Key.Item2, valore.Value)).ToList();
        }

        public int NumberOfElements
        {
            get
            {
                return values.Count;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
